package flatSearcher;
//TODO http://docs.oracle.com/javase/tutorial/uiswing/misc/systemtray.html
//TODO http://stackoverflow.com/questions/10967451/open-a-link-in-browser-with-java-button

import flatSearcher.ads.NeagentAd;
import flatSearcher.ads.OnlinerAd;
import flatSearcher.others.ConfigReader;
import flatSearcher.others.LogWriter;
import flatSearcher.searchers.NeagentSearcher;
import flatSearcher.searchers.OnlinerSearcher;
import flatSearcher.searchers.Searcher;

import java.awt.TrayIcon.MessageType;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;

public class FlatSearcher implements Runnable{
	private FlatSearcherGUI tray;
	private boolean isLogEnabled = false;
	private boolean notificationMode = false; //if set to true - the new tab will not appear, just a notification from tray
	private boolean onlinerValidation = true;
	private boolean neagentValidation = true;
	private LogWriter log;
	private ConfigReader configReader;
	
	public FlatSearcher() {
		try {
			configReader = new ConfigReader("conf.cfg");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isLogEnabled = Boolean.valueOf(configReader.getConfigs().get("log"));
		notificationMode = Boolean.valueOf(configReader.getConfigs().get("notification"));
		onlinerValidation = Boolean.valueOf(configReader.getConfigs().get("onlier"));
		neagentValidation = Boolean.valueOf(configReader.getConfigs().get("neagent"));
		tray = new FlatSearcherGUI();
		log = new LogWriter(isLogEnabled);
	}
	
	@Override
	public void run() {
		tray.addNewSearcher(new OnlinerSearcher("http://baraholka.onliner.by/viewforum.php?f=62&cat=5", "Onliner.by", "3"));
		tray.addNewSearcher(new NeagentSearcher("http://neagent.by/kvartira/snyat", "Neagent.by"));
		tray.start();
		while(true){
			for(Searcher searcher:tray.getSearchers()){
				try {
					searcher.getAdvertisements().clear();
					searcher.findAds();
					if(!searcher.getTheLastAd().equals(searcher.getAdvertisements().getLast().getUniqueID())){
						searcher.setTheLastAd(searcher.getAdvertisements().getLast().getUniqueID());
						log.print("New ad: "+searcher.getTheLastAd()+", "+searcher.getName());
						if(false){
							if(searcher.getAdvertisements().getFirst() instanceof NeagentAd){
								NeagentAd tempAd = (NeagentAd)searcher.getAdvertisements().getFirst();
								tray.getTrayIcon().displayMessage(tempAd.getTitle(),tempAd.getPrice()+"\n"+tempAd.getNumberOfRooms()+"\n"+tempAd.getAddress(),MessageType.INFO);
							}else if(searcher.getAdvertisements().getFirst() instanceof OnlinerAd){
								OnlinerAd tempAd = (OnlinerAd)searcher.getAdvertisements().getFirst();
								tray.getTrayIcon().displayMessage(tempAd.getTitle(),tempAd.getPrice()+"\n"+tempAd.getShortDescription(),MessageType.INFO);
							}
							tray.setUrlToOpen(searcher.getAdvertisements().getFirst().getUrl());
						}else
							tray.openWebpage(new URI(searcher.getAdvertisements().getFirst().getUrl()));
					}else
						System.out.println(searcher.getTheLastAd()+" "+searcher.getName());
					Thread.sleep(searcher.getSleepInterval());
				} catch (SocketTimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.print(e.getClass()+": "+e.getMessage()+", "+searcher.getName());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.print(e.getClass()+": "+e.getMessage()+", "+searcher.getName());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.print(e.getClass()+": "+e.getMessage()+", "+searcher.getName());
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					log.print(e.getClass()+": "+e.getMessage()+", "+searcher.getName());
				}
			}
		}
	}
}