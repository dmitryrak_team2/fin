package flatSearcher;

import flatSearcher.searchers.Searcher;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class FlatSearcherGUI implements ActionListener{
	
	private ArrayList<Searcher> searchers;
	private ArrayList<CheckboxMenuItem> checkboxes;
	private ArrayList<MenuItem> menuItems;
	private MenuItem exitItem = new MenuItem("Exit");
	private TrayIcon trayIcon;
	private String urlToOpen;
	
	public FlatSearcherGUI(){
		searchers = new ArrayList<Searcher>();
		checkboxes = new ArrayList<CheckboxMenuItem>();
		menuItems = new ArrayList<MenuItem>();
	}
	public void addNewSearcher(Searcher searcher){
		searchers.add(searcher);
		checkboxes.add(new CheckboxMenuItem(searcher.getName()+" validation"));
		menuItems.add(new MenuItem("open "+searcher.getName()));
	}
	public void start(){
		trayIcon = null;
		if (SystemTray.isSupported()) {
		    SystemTray tray = SystemTray.getSystemTray();
		    Image image = Toolkit.getDefaultToolkit().getImage("mage.jpg");
		    // create a popup menu
		    PopupMenu popup = new PopupMenu();
		    for(int i = 0;i<searchers.size();i++){
		    	checkboxes.get(i).setState(true);
		    	popup.add(checkboxes.get(i));
		    	popup.add(menuItems.get(i));
		    	checkboxes.get(i).addActionListener(this);
		    	menuItems.get(i).addActionListener(this);
		    	popup.addSeparator();
		    }
		    exitItem.addActionListener(this);
		    popup.add(exitItem);
		    trayIcon = new TrayIcon(image, "Flat Searcher", popup);
		    // set the TrayIcon properties
		    trayIcon.addActionListener(this);
		    // add the tray image
		    try {
		        tray.add(trayIcon);
		    } catch (AWTException e) {
		        System.err.println(e);
		    }
		}    
	}
	public TrayIcon getTrayIcon() {
		return trayIcon;
	}
	public void setTrayIcon(TrayIcon trayIcon) {
		this.trayIcon = trayIcon;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		 if(e.getSource().equals(exitItem)){
         	System.exit(0);
         }
		 if(e.getSource().equals(trayIcon)){
			 try {
				openWebpage(new URI(urlToOpen));
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 }
		 for(int i =0;i<searchers.size();i++){
			 if(e.getSource().equals(checkboxes.get(i))){
				 searchers.get(i).setSearchEnabled(!checkboxes.get(i).getState());
				 checkboxes.get(i).setState(!checkboxes.get(i).getState());
			 }
			 if(e.getSource().equals(menuItems.get(i))){
				try {
					System.out.println(searchers.get(i).getUrl());
					openWebpage(new URI(searchers.get(i).getUrl()));
				} catch (URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 }
		 }
	}
	public String getUrlToOpen() {
		return urlToOpen;
	}
	public void setUrlToOpen(String urlToOpen) {
		this.urlToOpen = urlToOpen;
	}
	public ArrayList<Searcher> getSearchers() {
		return searchers;
	}
	public void setSearchers(ArrayList<Searcher> searchers) {
		this.searchers = searchers;
	}
	public void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}
}