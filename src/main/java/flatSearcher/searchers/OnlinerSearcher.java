package flatSearcher.searchers;

import flatSearcher.ads.OnlinerAd;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class OnlinerSearcher extends Searcher{
	private String roomCount = "0,1,2,3,4,5,6";
	private String bDelimiter = "[", dDelimiteer = "]";
	private int priceMin = 500000, priceMax = 5000000;
	public String getRoomCount() {
		return roomCount;
	}
	//Constructor is mostly used to construct proper URL in accordance to the filters specified
	public OnlinerSearcher(String url, String searcherName, String roomCount){
		//TODO all the values should be taken from Config File
		super(url, searcherName);
		if(null !=roomCount && !("".equals(roomCount))){
			setRoomCount(roomCount);
		}
		//https://r.onliner.by/ak?bounds[lb][lat]=53.48294789710274&bounds[lb][long]=27.21887524609373&bounds[rt][lat]=54.309430162968496&bounds[rt][long]=27.905520753906227&page=1

		String [] flatTypeOneByOne = {};
		try{
			flatTypeOneByOne = getRoomCount().replaceAll("[^0-9,]", "").split(",");
		}catch(Exception ex){/*TODO catch exception if user have specified incorrect configs*/}
		for(int i = 0; i<flatTypeOneByOne.length;i++){
			if(Integer.valueOf(flatTypeOneByOne[i])<= 6){
				if("0".equals(flatTypeOneByOne[i])){
					url+="&rent_type[]="+"room";
				}else if("1".equals(flatTypeOneByOne[i])){
					url+="&rent_type[]="+flatTypeOneByOne[i]+"_room";
				}else{
					url+="&rent_type[]="+flatTypeOneByOne[i]+"_rooms";
				}
			}
		}
		
		//&rent_type[]=1_room  &price[min]=500000&price[max]=20000000&only_owner=true
		//TODO price max and price min 
		if(!isPremiumEnabled){
			url+="&only_owner="+"true";
		}
		setUrl(url);
	}
	public void setRoomCount(String roomCount) {
		this.roomCount = roomCount;
	}
	@Override
	public void findAds() throws IOException, SocketTimeoutException{
		
		if(isSearchEnabled){
			Document doc = Jsoup.connect(url).get();
			Elements ads = doc.getElementsByClass("frst");
			for (Element element:ads){
				if(!isPremiumEnabled){
					if(element.parent().className().equals("m-imp")){ //to skip premium ADs
						continue;
					}
				}
				OnlinerAd newAd = new OnlinerAd();
				newAd.setUrl(element.getElementsByClass("wraptxt").get(0).getElementsByAttribute("href").attr("href"));
				newAd.setUniqueID(element.getElementsByClass("wraptxt").get(0).getElementsByAttribute("href").attr("href"));
				newAd.setTitle(element.getElementsByClass("wraptxt").get(0).getElementsByAttribute("href").get(0).ownText());
				if(element.getElementsByClass("ba-description").size()>0){
					newAd.setShortDescription(element.getElementsByClass("ba-description").get(0).ownText());
				}else{
					newAd.setShortDescription(thumb);
				}
				if(element.nextElementSibling().getElementsByTag("strong").size()>0){
					String priceToInt = element.nextElementSibling().getElementsByTag("strong").get(0).ownText().replaceAll( "[^\\d]", "" ); //replace all the non-numerical
					newAd.setPrice(Integer.parseInt(priceToInt)); //to get price
				}
				advertisements.add(newAd);
			}
		}
	}
}
