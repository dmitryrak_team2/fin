package flatSearcher.searchers;

import flatSearcher.ads.Ad;
import flatSearcher.others.LogWriter;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.LinkedBlockingDeque;

public abstract class Searcher {
	protected String url;
	protected String name;
	protected LinkedBlockingDeque<Ad> advertisements;
	protected String theLastAd = "";
	protected String thumb = "NULL";
	protected int sleepInterval = 5000;
	protected boolean isSearchEnabled = true;
	protected LogWriter log = new LogWriter(false);
	protected boolean isPremiumEnabled = false;
	
	public Searcher(String url,String searcherName){
		this.url = url;
		this.name = searcherName;
		this.advertisements = new LinkedBlockingDeque<Ad>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public LinkedBlockingDeque<Ad> getAdvertisements() {
		return advertisements;
	}
	public void setAdvertisements(LinkedBlockingDeque<Ad> advertisements) {
		this.advertisements = advertisements;
	}
	public abstract void findAds() throws IOException, SocketTimeoutException;

	public String getTheLastAd() {
		return theLastAd;
	}
	public void setTheLastAd(String theLastAd) {
		this.theLastAd = theLastAd;
	}
	public int getSleepInterval() {
		return sleepInterval;
	}
	public void setSleepInterval(int sleepInterval) {
		this.sleepInterval = sleepInterval;
	}
	public boolean isSearchEnabled() {
		return isSearchEnabled;
	}
	public void setSearchEnabled(boolean isSearchEnabled) {
		this.isSearchEnabled = isSearchEnabled;
	}
}
