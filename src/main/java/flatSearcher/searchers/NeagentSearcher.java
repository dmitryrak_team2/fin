package flatSearcher.searchers;

import flatSearcher.ads.NeagentAd;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class NeagentSearcher extends Searcher{
	
	public NeagentSearcher(String url, String searcherName){
		super(url,searcherName);
	}
	@Override
	public void findAds() throws IOException, SocketTimeoutException {
		if(isSearchEnabled){
			Document doc = Jsoup.connect(url).get();
			Elements ads = doc.getElementsByClass("itm");
			for (Element element:ads){
				if(!isPremiumEnabled)
					if(element.className().equals("itm  up")){ // to skip premium ADs
						continue;
					}
				NeagentAd newAd = new NeagentAd();
				newAd.setUniqueID(element.id()); //to get unique ID
				newAd.setUrl(element.getElementsByTag("a").get(0).attr("href")); //to get link
				newAd.setAddress(element.getElementsByClass("itm_street").get(0).ownText());
				String priceToInt = element.getElementsByClass("itm_price").get(0).ownText().replaceAll( "[^\\d]", "" ); //replace all the non-numerical
				newAd.setPrice(Integer.parseInt(priceToInt)); //to get price
				newAd.setTitle(element.getElementsByTag("h2").get(0).ownText());
				newAd.setNumberOfRooms(element.getElementsByClass("itm_komnat").get(0).ownText());
				newAd.setShortDescription(element.ownText());
				advertisements.add(newAd);
			}
		}
	}
}
