package flatSearcher.searchers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;

public class KvartirantSearcher extends Searcher{

	public KvartirantSearcher(String url, String searcherName) {
		super(url, searcherName);
	}
	@Override
	public void findAds() throws IOException, SocketTimeoutException {
		// TODO Auto-generated method stub	
		Document doc = Jsoup.connect(url).userAgent("Chrome").get(); //to avoid anti-robot
		Elements ads = doc.getElementsByClass("paid");
		System.out.println(ads);
		
	}
	public static void main(String[] args) throws SocketTimeoutException, IOException{
		KvartirantSearcher s = new KvartirantSearcher("http://www.kvartirant.by/ads/flats/type/rent/?tx_uedbadsboard_pi1%5Bsearch%5D%5Bowner%5D=on","Kvartirant");
		s.findAds();
	}
}
