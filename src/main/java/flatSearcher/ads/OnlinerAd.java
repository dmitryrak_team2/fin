package flatSearcher.ads;

/**
 * 
 * @author by_drak
 * @version 1.00
 */

public class OnlinerAd extends Ad{
	private String domainName = "http://baraholka.onliner.by";

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getDomainName() {
		return domainName;
	}
	public void setUrl(String url){
		url = url.substring(1); //to remove "." from the start
		this.url = this.domainName+url; //to make absolute path
	}
}
